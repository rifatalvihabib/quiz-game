; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt
jumps
.MODEL SMALL
org 100h

.stack 100h
.data 
   namemsg db "Username :",'$'     
   passmsg db "Passsword :",'$'
   valid db "Login successful! ",0ah,0dh,'$'
   invalid db "Invalid try again",0ah,0dh,'$'
   
   
   
   
MSG1 DB '                .....WELCOME TO YOUR QUIZ GAME.....$'
MSG2 DB 'Rules : $'
MSG3 DB '*. For Every Correct answer you will get 1 point.$'
MSG4 DB '*.WHICH LANGUAGE: .$'  
QCHOICE1 DB '   a) C LANGUAGE    b) ASSEMBLY$' 

TRY DB '  WRONG CHOICE .TYPE AGAIN : $'



                                

Q1 DB '1.What is the size of float in bits?$'
QA1 DB '   a) 32    b) 64    c) 16$'
Q2 DB '2.If a=2,x=--a,x=?$'
QA2 DB '   a) 0    b) 1    c) -1$'
Q3 DB '3.%i is also used for?$'
QA3 DB '   a) double    b) int    c) float$'
Q4 DB '4.To get length of string we use- $'
QA4 DB '   a) strlen    b) lenstr    c) strcmp$'
Q5 DB '5.If x=4,y=2,z=x%y,z=?$'
QA5 DB '   a) 2    b) 0    c) 1$'
Q6 DB '1.To print a string in int 21h  what do we use?$'
QA6 DB '   a) lea dx    b)mov ah    c)mov dx$'
Q7 DB '2.Which interrupt do we use in graphics mode? $'
QA7 DB '   a) int 21h    b)int 10h    c) int 11h$'
Q8 DB '3. Which is a 16bit register?$'
QA8 DB '   a) ah    b) al    c)ax $'
Q9 DB '4. How many addressing mode are there?$'
QA9 DB '   a) 7    b) 6    c) 8$'
Q10 DB '5.In int 21h to take input which is correct?$'
QA10 DB '   a)mov ah,9    b) mov ah,1    c) mov ah,2$' 


  
MSG6 DB '  Right Answer....$'
MSG7 DB '  Wrong Answer....$'   


CNT1 DB 0 
CNT2 db 0
MSG8 DB 'You have successfully completed your quiz.$'
MSG9 DB 'Your Total obtained point is : $' 
MSG10 DB 'Press 1 to Re-attempt quiz or 0 to Exit.$'
   
   
   blank db 0ah,0dh,'$'
   
   CKRSI DB 'AMRA C LANGUAGE KORECHI',0ah,0dh,'$'     
   
   JAVAKRSI DB 'AMRA JAVA LANGUAGE KORSI',0ah,0dh,'$'
   
   count db 0  
.code 

 proc main  
    
     mov dx,@data
     mov ds,dx
   man:
     lea dx,namemsg      ;print Enter name
     mov ah,9
     int 21h
     
       
    l1:
    
         mov ah, 1          ;type username
         int 21h
    
         
    
         cmp al, 0dh
         je l2
        
         
         push ax
         inc count 
         jmp l1
   
    
    
    l2:  
        
        pop bx
         cmp bl, 't'
        jne l4
        
        pop bx
        cmp bl, 'a'
        jne l4
        
        pop bx
        cmp bl, 'f'
        jne l4
        
        pop bx
        cmp bl, 'i'
        jne l4
        
        pop bx
        cmp bl, 'r'
        jne l4
        
       
        je   l6
        
       
     
        
    l4:
         
        cmp bl, 'y'
        jne l5
        
        pop bx
        cmp bl, 't'
        jne l5
        
        pop bx
        cmp bl, 'i'
        jne l5
        
        pop bx
        cmp bl, 'a'
        jne l5
        
        pop bx
        cmp bl, 'h'
        jne l5
        
        pop bx
        cmp bl, 'c'
        jne l5 
        
        je  l61
    
        
    l5:   
    
     lea dx, blank
        mov ah, 9
        int 21h
    
        lea dx, invalid
        mov ah, 9
        int 21h
        jmp man
        
    l6:     lea dx, blank
        mov ah, 9
        int 21h
    
        lea dx,passmsg
     mov ah,9
     int 21h
      
      mov count,0
       
    l7:
    
         mov ah, 8
         int 21h
    
         
    
         cmp al, 0dh
         je l8
        
         
         push ax
         inc count 
         jmp l7
        
        
        
   
    
    l8:  
        
        pop bx
         cmp bl, 'i'
        jne l10
        
        pop bx
        cmp bl, 'i'
        jne l10
        
        pop bx
        cmp bl, 'v'
        jne l10
        
        pop bx
        cmp bl, 'l'
        jne l10
        
        pop bx
        cmp bl, 'a'
        jne l10
        
       
        je   l11
        
       
    
   
   
    
    
   l61:     lea dx, blank
        mov ah, 9
        int 21h
    
        lea dx,passmsg
     mov ah,9
     int 21h
      
      mov count,0
       
    l71:
    
         mov ah, 8
         int 21h
    
         
    
         cmp al, 0dh
         je l91
        
         
         push ax
         inc count 
         jmp l71
        
        
        
   
    
   
        
       
     
        
    l91:
        pop bx 
        cmp bl, 'n'
        jne l10
        
        pop bx
        cmp bl, 'r'
        jne l10
        
        pop bx
        cmp bl, 'a'
        jne l10
        
        pop bx
        cmp bl, 'p'
        jne l10
        
        pop bx
        cmp bl, 'u'
        jne l10
        
        pop bx
        cmp bl, 's'
        jne l10 
        
        je  l11
   
   
   
   
     
      l10:   
    
        lea dx, blank
        mov ah, 9
        int 21h
    
        lea dx, invalid
        mov ah, 9
        int 21h
        jmp man
        
    l11:  lea dx, blank
          mov ah, 9
          int 21h
    
          lea dx,valid
          mov ah,9
          int 21h
   
      ;jmp exit
         jmp choices
        
   
   
   choices:
         
	
   
    LEA DX,MSG1
	MOV AH,9
	INT 21H
	
	CALL NL
    
	LEA DX,MSG2
	MOV AH,9
	INT 21H
    
	CALL NL
    
	LEA DX,MSG3
	MOV AH,9
	INT 21H
    
    CALL NL
    
	
     
     

         
         
         
         
         
      ASHOL:
      
      
     MOV CNT1,0  
     MOV CNT2,0
     
    LEA DX,MSG4
	MOV AH,9
	INT 21H  
	CALL NL
	
    LEA DX,QCHOICE1
	MOV AH,9
	INT 21H 
	CALL NL
     
     MOV AH,1
     INT 21H
     
       
     CMP AL,'a'
     JE OPTN1
     JNE NOKOL
     
     
     NOKOL:
     CMP AL,'b'
     JE OPTN2
     JNE TRYAGN
     
     
     TRYAGN:    
     
     
               CALL NL
               
     LEA DX,TRY
     MOV AH,9
     INT 21H 
        CALL NL
     JMP ASHOL
     
     
     OPTN1:
     
     QS1:
       CALL NL  
       LEA DX,Q1
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA1
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'a'
     je  RIGHT1
     jne WRONG1
       
        RIGHT1:   
                INC CNT1
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS2
	      
	      WRONG1:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS2
     
     
      
     QS2:
       CALL NL  
       LEA DX,Q2
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA2
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'b' 
	     je  RIGHT2
     jne WRONG2
       
        RIGHT2:  INC CNT1
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS3
	      
	      WRONG2:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS3
       
     
     
     
      QS3:
       CALL NL  
       LEA DX,Q3
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA3
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'c'
     je  RIGHT3
     jne WRONG3
       
        RIGHT3:  INC CNT1
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS4
	      
	      WRONG3:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS4 
	              
	   
	   
	    QS4:
       CALL NL  
       LEA DX,Q4
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA4
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'a'
     je  RIGHT4
     jne WRONG4
       
        RIGHT4:  INC CNT1
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS5
	      
	      WRONG4:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS5
	              
	              
	              
	    QS5:
       CALL NL  
       LEA DX,Q5
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA5
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'b'
     je  RIGHT5
     jne WRONG5
       
        RIGHT5:  INC CNT1
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP CNT11
	      
	      WRONG5:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP CNT11
	              
	              
	   CNT11:    call NL 
	                LEA DX,MSG8
	              MOV AH,9
	              INT 21H      
	                CALL NL
	          LEA DX,MSG9
	              MOV AH,9
	              INT 21H  
	              
	              MOV AL,CNT1
	              ADD AL,30H
	              MOV DL,AL
	              MOV AH,2
	              INT 21H
	              
	
	JMP ending
     
          
      
     OPTN2:
     
     QS6:
       CALL NL  
       LEA DX,Q6
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA6
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'a'
     je  RIGHT6
     jne WRONG6
       
        RIGHT6:   
                INC CNT2
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS7
	      
	      WRONG6:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS7
     
     
      
     QS7:
       CALL NL  
       LEA DX,Q7
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA7
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'b' 
	     je  RIGHT7
     jne WRONG2
       
        RIGHT7:  INC CNT2
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS8
	      
	      WRONG7:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS8
       
     
     
     
      QS8:
       CALL NL  
       LEA DX,Q8
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA8
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'c'
     je  RIGHT8
     jne WRONG8
       
        RIGHT8:  INC CNT2
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS9
	      
	      WRONG8:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS9 
	              
	   
	   
	    QS9:
       CALL NL  
       LEA DX,Q9
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA9
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'a'
     je  RIGHT9
     jne WRONG9
       
        RIGHT9:  INC CNT2
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP QS10
	      
	      WRONG9:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP QS10
	              
	              
	              
	    QS10:
       CALL NL  
       LEA DX,Q10
	   MOV AH,9
	   INT 21H 
	
	   CALL NL
	   
	    LEA DX,QA10
    	MOV AH,9
    	INT 21H
    	 
    	CALL NL 
    	
    	  MOV AH,1
     INT 21H
    	
	
	     CMP AL,'b'
     je  RIGHT10
     jne WRONG10
       
        RIGHT10:  INC CNT2
                LEA DX,MSG6
	            MOV AH,9
	            INT 21H
	            JMP CNT12
	      
	      WRONG10:  LEA DX,MSG7
	              MOV AH,9
	              INT 21H
	              JMP CNT12
	              
	              
	   CNT12:    call NL                  
	              
	          LEA DX,MSG9
	              MOV AH,9
	              INT 21H  
	              
	              MOV AL,CNT2
	              ADD AL,30H
	              MOV DL,AL
	              MOV AH,2
	              INT 21H
	              
	
	JMP ending2
      
         
         
         
   
   
   proc NL
    
    LEA DX,blank 
	MOV AH,9
	INT 21H
	ret
endp NL
   
   
   
   
       ending:
       
       call NL
       
       
        LEA DX,MSG10
	            MOV AH,9
	            INT 21H
	            
	            
	            CALL NL
	            
	            MOV AH,1
	            INT 21H
	            CMP AL,'1'
	            JE ASHOL
	            CMP AL,'0'
	            JMP exit         
	            
	            
	            
	            
	            
	             ending2:
       
       call NL
       
       
        LEA DX,MSG10
	            MOV AH,9
	            INT 21H
	            
	            
	            CALL NL
	            
	            MOV AH,1
	            INT 21H
	            CMP AL,'1'
	            JE ASHOL
	            CMP AL,'0'
	            JMP exit2
	            
	            
	            
	            
	            
	            
	            
	            
	            
	            
	            
         
   
    exit:     
               ADD CNT1,30H
               
               
               ;ADD CNT2,30H
    
    
               CMP CNT1, '0'
               JE  SCORE0
               
               CMP CNT1, '1'
               JE  SCORE1
               
               CMP CNT1, '2'
               JE  SCORE2
               
               CMP CNT1, '3'
               JE  SCORE3
               
               CMP CNT1, '4'
               JE  SCORE4
               
               CMP CNT1, '5'
               
               JE  SCORE5    
               jmp EXIT1
               
               
        exit2:       
               
                ADD CNT2,30H
    
    
               CMP CNT2, '0'
               JE  SCORE0
               
               CMP CNT2, '1'
               JE  SCORE1
               
               CMP CNT2, '2'
               JE  SCORE2
               
               CMP CNT2, '3'
               JE  SCORE3
               
               CMP CNT2, '4'
               JE  SCORE4
               
               CMP CNT2, '5'
               JE  SCORE5
               
               
         JMP EXIT1      
               
               
                
                  
   
    
               
               
  SCORE5:
            
            mov ah, 0     ;onno graphics mood
    mov al, 12h   ;640*480
    int 10h
     
  mov cx, 10 
  mov dx, 10
    
  K1:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
   ; mov dx, 10    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx, 70 
    
    
    
    
    jle K1
    
  mov cx, 10
  mov dx, 60
    
   K2:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
   ; mov cx, 10    ; x= 10
    ;mov dx, 0    ; y = 10
    int 10h 
    
    inc cx
    cmp cx,70 
    
    
    
    
    jle K2 
    
    mov cx, 10
    mov dx, 110
     K3:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
    mov dx, 110    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx,70  
    
    
     jle K3
    
   mov cx, 70
   mov dx, 10 
      
   mov cx, 10 
  mov dx, 10
    
  K4:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
   ; mov dx, 10    ; y = 10
    int 10h 
    inc dx
    ;inc dx
    cmp dx, 60 
    
    
    
    
    jle K4
    
    
    
    mov cx, 70 
  mov dx, 60
    
  K5:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
   ; mov dx, 10    ; y = 10
    int 10h 
    ;inc cx
    inc dx
    cmp dx, 110 
    
    
    
    
    jle K5
     
    
  
      
    JMP EXIT1  
    
    
    
    
    SCORE4:
    
    
    mov ah, 0     ;onno graphics mood
    mov al, 12h   ;640*480
    int 10h
     
  mov cx, 10 
  mov dx, 10
    
  a1:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
   ; mov dx, 10    ; y = 10
    int 10h 
    inc dx
    ;inc dx
    cmp dx, 60 
    
    
    
    
    jle a1
    
  mov cx, 10
  mov dx, 60
    
   a2:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
   ; mov cx, 10    ; x= 10
    ;mov dx, 0    ; y = 10
    int 10h 
    
    inc cx
    cmp cx,70 
    
    
    
    
    jle a2 
    
    mov cx, 10
    mov dx, 110
    ; l3:
    
   ; mov ah, 0ch
   ; mov al, 15    ; al = 15 white
    ;;mov cx, 10    ; x= 10
   ; mov dx, 110    ; y = 10
   ; int 10h 
   ; inc cx
    ;inc dx
   ; cmp cx,70  
    
    
    ; jle l3
    
   mov cx, 70
   mov dx, 10 
      
   a4:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 110    ; x= 10
    ;mov dx, 10    ; y = 10
    int 10h 
    
    inc dx
    cmp dx, 110 
    
    
    
    
    jle a4 
      
    
    
    JMP EXIT1
              
              
              
              
              
              
    
         SCORE3:    
             
             
             mov ah, 0     ;onno graphics mood
    mov al, 12h   ;640*480
    int 10h
     
  mov cx, 10 
  mov dx, 10
    
  B1:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
   ; mov dx, 10    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx, 70 
    
    
    
    
    jle B1
    
  mov cx, 10
  mov dx, 60
    
   B2:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
   ; mov cx, 10    ; x= 10
    ;mov dx, 0    ; y = 10
    int 10h 
    
    inc cx
    cmp cx,70 
    
    
    
    
    jle B2 
    
    mov cx, 10
    mov dx, 110
     B3:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
    mov dx, 110    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx,70  
    
    
     jle B3
    
   mov cx, 70
   mov dx, 10 
      
   B4:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 110    ; x= 10
    ;mov dx, 10    ; y = 10
    int 10h 
    
    inc dx
    cmp dx, 110 
    
    
    
    
    jle B4 
             
       JMP EXIT1        
       
              
              
              
              
              
              
              
              
              
              
              
              
              SCORE2:
              
              
             

    mov ah, 0     ;onno graphics mood
    mov al, 12h   ;640*480
    int 10h
     
  mov cx, 10 
  mov dx, 10
    
  C1:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
   ; mov dx, 10    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx, 70 
    
    
    
    
    jle C1
    
  mov cx, 10
  mov dx, 60
    
   C2:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
   ; mov cx, 10    ; x= 10
    ;mov dx, 0    ; y = 10
    int 10h 
    
    inc cx
    cmp cx,70 
    
    
    
    
    jle C2 
    
    mov cx, 10
    mov dx, 110
     C3:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
    mov dx, 110    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx,70  
    
    
     jle C3
    
   mov cx, 70
   mov dx, 10 
      
   C4:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 110    ; x= 10
    ;mov dx, 10    ; y = 10
    int 10h 
    
    inc dx
    cmp dx, 60 
    
    
    
    
    jle C4 
    
   mov cx, 10
   mov dx, 60 
    
    
    C5:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 110    ; x= 10
    ;mov dx, 10    ; y = 10
    int 10h 
    
    inc dx
    cmp dx, 110 
    
    
    
    
    jle C5 
     JMP EXIT1
              
              
              
              
              
   
   
   
   SCORE1:
   
            
            
            
             mov ah, 0     ;onno graphics mood
    mov al, 12h   ;640*480
    int 10h
     

    
   mov cx, 70
   mov dx, 10 
      
   M4:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 110    ; x= 10
    ;mov dx, 10    ; y = 10
    int 10h 
    
    inc dx
    cmp dx, 110 
    
    
    
    
    jle M4 
   
   
  
      
          
          JMP EXIT1
          
          
     
     
     SCORE0:
     
              mov ah, 0     ;onno graphics mood
    mov al, 12h   ;640*480
    int 10h
     
  mov cx, 10 
  mov dx, 10
    
  Z1:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
   ; mov dx, 10    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx, 70 
    
    
    
    
    jle Z1
    
  mov cx, 10
  mov dx, 60
    
  
    
    mov cx, 10
    mov dx, 110
     Z3:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 10    ; x= 10
    mov dx, 110    ; y = 10
    int 10h 
    inc cx
    ;inc dx
    cmp cx,70  
    
    
     jle Z3
    
   mov cx, 70
   mov dx, 10 
      
   Z4:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 110    ; x= 10
    ;mov dx, 10    ; y = 10
    int 10h 
    
    inc dx
    cmp dx, 110 
    
    
    
    
    jle Z4 
    
    
   mov cx, 10
   mov dx, 10
             
             
     Z5:
    
    mov ah, 0ch
    mov al, 15    ; al = 15 white
    ;mov cx, 110    ; x= 10
    ;mov dx, 10    ; y = 10
    int 10h 
    
    inc dx
    cmp dx, 110 
    
    
    
    
    jle Z5 
     
     
     
     
     
     
     
     
     
     
     JMP EXIT1
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
   
   
   EXIT1:

  endp  main
 end main